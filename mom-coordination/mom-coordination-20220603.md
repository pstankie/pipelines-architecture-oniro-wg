coordination meeting 20220603

## Participants

* Pavel S. (Huawei)
* Sebastien H. (EF)
* Agustin B.B. (EF)
* Mikael B. (EF)

## Agenda

* Show project documentation support
* Define a migration strategy
* Set up the schedule for this meeting series

### Show project documentation support

Documentation available under: https://gitlab.eclipse.org/eclipse-wg/oniro-wg/roadmap-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg

### Define a migration strategy

Project priority: 
- HLaas
- eddie
- oniro
- eddie 
- docs
- meta-* (meta-openharmony, meta-zephyr)
- blueprint when sources will be available (to reschedule)

Strategy propose : 

Deactivated runner on HLaas for testing runner.

Other proposal : 

* Create gitlab branch migration per project
* Maybe specify branches by feature build
* If necessary, with the use of runner tags! ex : project `oniro`, `build`, `test`, ...

### Set up the schedule for this meeting series

Pawel busy planning:
    Mon 11- 12
    Tue 12-12.30
    Wed 11-12
    Thu – 10-11 

### Action

* Sebastien H.
  * Organize a regular meeting with Pawel in order to complete these documents on infrastructure and configuration pipelines.
  * Continue to organize and populate with all known pipelines information.  
* Pawel S. (not discuss in meeting), if you can start to take a look at all topics concerned
